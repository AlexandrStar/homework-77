import axios from '../../axios-api';
import {CREATE_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "./actionsType";

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});

export const fetchPosts = () => {
  return dispatch => {
    return axios.get('/posts').then(
      response => dispatch(fetchPostsSuccess(response.data))
    );
  };
};

export const createPost = postData => {
  return dispatch => {
    return axios.post('/posts', postData).then(
      () => dispatch(createPostSuccess())
    );
  };
};