import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import Posts from "./containers/Posts/Posts";
import NewPost from "./containers/NewPost/NewPost";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Toolbar/>
        </header>
        <Container>
          <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/posts/new" exact component={NewPost}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
