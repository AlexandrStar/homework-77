import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {fetchPosts} from "../../store/actions/actionPost";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import PostThumbnail from "../../components/PostThumbnail/PostThumbnail";

class Posts extends Component {

  componentDidMount() {
    this.props.onFetchPosts();
  }

  render() {
    return (
      <Fragment>
        <h2>
          Posts
          <Link to="/posts/new">
            <Button
              color="primary"
              className="float-right"
            >
              Add post
            </Button>
          </Link>
        </h2>

        {this.props.posts.map(post => (
          <Card key={post.id} style={{marginTop: '10px', backgroundColor: '#DCDCDC'}}>
            <CardBody>
              <PostThumbnail image={post.image} />
              <strong style={{color: 'blue', marginLeft: '10px'}}>
                {!post.author ? <strong style={{marginLeft: '10px'}}>Anonymous</strong>: post.author}
              </strong>
              <strong style={{marginLeft: '10px'}}>
                {post.message}
              </strong>
            </CardBody>
          </Card>
        ))}

      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts,
});

const mapDispatchToProps = dispatch => ({
  onFetchPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);