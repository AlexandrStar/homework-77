import React from 'react';
import {apiURL} from "../../constans";

const style = {
  width: '100px',
  height: '100px',
  marginRight: '10px'
};

const PostThumbnail = props => {
  if (props.image !== 'null') {
    let image = apiURL + '/uploads/' + props.image;
    return <img src={image} style={style} className="img-thumbnail" alt="Post image" />;
  }
  return null

};

export default PostThumbnail;