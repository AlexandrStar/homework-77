const express = require('express');
const multer = require('multer');
const path = require('path');
const fileDb = require('../fileDb');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
  res.send(fileDb.getItems());
});

router.get('/:id', (req, res) => {
  res.send('A single post by id will be here');
});

router.post('/', upload.single('image'), (req, res) => {
  if (!req.body.message) {
    res.status(400).send({error: "Author and message must be present in the request"})
  } else {
    const post = req.body;
    post.id = nanoid();

    if (req.file) {
      post.image = req.file.filename;
    }

    fileDb.addItem(post);
    res.send({message: 'OK'});
  }
});

module.exports = router;