const express = require('express');
const cors = require('cors');
const posts = require('./app/posts');
const fileDb = require('./fileDb');
const app = express();
fileDb.init();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

app.use('/posts', posts);

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});